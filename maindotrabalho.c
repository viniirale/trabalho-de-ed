#include<stdio.h>
#include<stdlib.h>
#include"trabalho.h"
#include<time.h>
#include<ctype.h>
#include<string.h>
#include<limits.h>
#define VET 15

int main( int argc, char *argv[ ] )
{


    FILE *arquivo;                          //ponteiro para o arquivo com os comandos
    FILE *arqabp;
    FILE *arqavl;
    char nomearqcomandos[VET]= {"trab.txt"};             //nomearquivo de comandos
    char op,aux,nomearqoperado[VET];            //sigla da opera��o a ser feita com o arquivo, nome do arquivo a ser operado
    pNodoA *abp;                            //arvore de pesquisa
    pNodoB *avl;
    int *rot;
    unsigned long int *comp;
    long int tempo=0, *aux_temp;
    aux_temp= &tempo;
    int codigo=0;
    clock_t start, end, elapsed; //para contar o tempo
    int flag=0;

    // arquivo=fopen("argv[0]","r");         descomentar quando for testar no dosbox
    abp=cria_arvore();
    //avl=cria_arvore();
    arquivo=fopen(nomearqcomandos,"r");
    arqabp=fopen("Saida_ABP","w+");
    arqavl=fopen("Saida_AVL","w+");
    //**********************************************

    do
    {
        op=fgetc(arquivo);

        if(op!='e'&& op!='E')
            fscanf(arquivo,"%s\n",nomearqoperado);
        else
            aux=fgetc(arquivo);
        switch(toupper(op))
        {
        case 'I':
            abp=controlainsereArvoreabp(abp,nomearqoperado,&comp,&tempo);
            //printf("estou aqui\n");
            //exit(0);
            // avl=controlainsereArvoreavl(avl,nomearqoperado,&comp,&tempo,&rot);
            codigo=1;
            break;
        case 'E':
            Inserearquivoabp(abp,codigo, nomearqoperado,&flag,arqabp,&comp,&tempo);
            // Inserearquivoavl(avl,codigo, nomearqoperado,&flag,arqavl,&comp,&tempo,&rot);
            break;
        case 'C':
            controlaconsulta(&abp,nomearqoperado,&comp,&tempo);
            //controlaconsultaavl(&avl,nomearqoperado,&comp,&tempo,&rot);

            codigo=2;
            break;
        case 'R':
            controlaremoveABP(&abp,nomearqoperado,&comp,&tempo);
            //controlaremoveavl(&avl,nomearqoperado,&comp,&tempo,&rot);
            codigo=3;
            break;
        }
    }
    while(!feof(arquivo));



    fclose(arquivo);
    fclose(arqabp);
    fclose(arqavl);
    abp=destroi(abp);
    return 0;
}
