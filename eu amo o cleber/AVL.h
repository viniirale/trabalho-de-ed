#pragma once

#include <stdio.h>
#include <stdlib.h>  
#include <time.h>
#include <string.h>
#include <windows.h>

#include <iostream>
#include <fstream>


#include <math.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glut.h>
#include "glui.h"
#include "Files.h"
#include "Main.h"



#ifdef __cplusplus
#define EXTERNC extern "C"
#else
#define EXTERNC
#endif

#define AVL_API DLL_IMPORT


struct TNodo
{
        int info;
        int FB;              
        struct TNodo *esq;        
        struct TNodo *dir;        
};
extern TNodo* pNodoA;

TNodo* InsereAVL(TNodo* pNodo, int ch);
TNodo* Consulta(TNodo* pNodo, char ch);
TNodo* Remove(TNodo* pNodo, char ch);
TNodo* RemoveAVL(TNodo* pNodo);

int Calcula_FB(TNodo* pNodo);
void Seta_FB(TNodo* pNodo);
TNodo* CorrigeAVL(TNodo* pNodo);

TNodo* rotacao_direita(TNodo* pNodo);
TNodo* rotacao_esquerda(TNodo* pNodo);
TNodo* rotacao_dupla_direita (TNodo* pNodo);
TNodo* rotacao_dupla_esquerda (TNodo* pNodo);

