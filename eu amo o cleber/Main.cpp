#include "AVL.h"

float	xy_aspect;
int		main_window;
int		set_texto=0;
int		nodo_adcionar = 0;
int		nodo_remover = 0;
int		nodo_consultar = 0;


GLUI *glui,			*obj_glui;
GLUI_Panel			*obj_arquivo;
GLUI_Panel			*obj_exibir;
GLUI_Panel			*obj_adcionar;
GLUI_Panel			*obj_remover;
GLUI_Panel			*obj_consultar;
GLUI_Panel			*obj_sair;
GLUI_RadioGroup		*obj_set_texto;


void glDrawText(GLfloat x, GLfloat y, char* s, GLfloat r, GLfloat g, GLfloat b)
{
	int lines;
    char* p;

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
		glLoadIdentity();
		glOrtho(0.0, glutGet(GLUT_WINDOW_WIDTH), 0.0, glutGet(GLUT_WINDOW_HEIGHT), -1.0, 1.0);
		glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
			
	glLoadIdentity();
	glColor3f(r,g,b);
	glRGB(255, 0, 0);
	glRasterPos2i(x, y);
	for(p = s, lines = 0; *p; p++)
	{
		if (*p == '\n')
		{
			lines++;
			glRasterPos2i(x, y-(lines*18));
		}
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *p);
	}
	
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

}

void DrawNodo(TNodo* pNodo, GLfloat PosX, GLfloat PosY, GLfloat PosZ)
{

	char str[3]  = { 'a', 'a', 'a'};
	char Strs[10] = {'0','1','2','3','4','5','6','7','8','9'};
	int i =0;
	int n =0;

	if(set_texto==0)
	{	
		n=pNodo->info;
		while( n >= 10)
		{
			i++;
			n-=10;
		}
		str[0]=Strs[i];
		str[1]=Strs[n];
		str[2]=0;
	}
	else
	{
		n=pNodo->FB;
		if(n == 0)
		{
			str[0]=' ';
			str[1]='0';
			str[2]=0;
		}
		else if(n < 0)
		{
			str[0]='-';
			str[1]=Strs[-1*n];
			str[2]=0;
		}
		else if(n > 0)
		{
			str[0]='+';
			str[1]=Strs[n];
			str[2]=0;
		}
	}

	glRGB(0, 0, 255);
	glPushMatrix();
	glTranslatef(PosX, PosY, PosZ);
	glutSolidSphere(0.05f, 20, 20);
	
	glDrawText(400*(1+PosX)-11, 300*(1+PosY)-5,str, 1, 0, 0);
	
	glPopMatrix();

}


void DrawNodos(TNodo* pNodo, GLfloat PosX, GLfloat PosY, GLfloat PosZ, int n)
{
     if (pNodo!= NULL)
     {
			DrawNodo(pNodo,PosX,PosY,PosZ);
		    n++;

			if(pNodo->esq!=NULL)
			{
				glBegin(GL_LINES);
					glVertex2f(PosX,PosY-0.05);
					glVertex2f((PosX-(1/pow(2,n))), (PosY-(1/pow(2,n))-0.1));
				glEnd();
				DrawNodos(pNodo->esq,(PosX-(1/pow(2,n))), (PosY-(1/pow(2,n))-0.1), PosZ,n);
			}

			if(pNodo->dir!=NULL)
			{
				glBegin(GL_LINES);
					glVertex2f (PosX,PosY-0.05);
					glVertex2f ((PosX+(1/pow(2,n))), (PosY-(1/pow(2,n))-0.1));
				glEnd();
				DrawNodos(pNodo->dir,(PosX+(1/pow(2,n))), (PosY-(1/pow(2,n))-0.1), PosZ,n);
			}
      }
}

																							
void control_cb( int control )
{
	TNodo* pNodo=NULL;

	if ( control == ABRIR_ID )
	{
		if(!carrega_csv())
		{
			MessageBox(NULL,"Erro ao abrir nodos.txt!","Error",MB_OK | MB_ICONEXCLAMATION);
		}
	}
	else if ( control == EXIBIR_ID )
	{
		obj_glui->disable();
	}
	else if ( control == ADCIONAR_ID )
	{
		pNodoA = InsereAVL(pNodoA, nodo_adcionar);
		pNodoA = CorrigeAVL(pNodoA);
		Seta_FB(pNodoA);
	}
	else if ( control == REMOVER_ID )
	{
		pNodoA = Remove(pNodoA,nodo_remover);
		pNodoA = CorrigeAVL(pNodoA);
		Seta_FB(pNodoA);		
	}
	else if ( control == CONSULTAR_ID )
	{
		pNodo = Consulta(pNodoA,nodo_consultar);
		if(pNodo!=NULL)
		{
			MessageBox(NULL,"O Nodo foi encontrado!!! ","ARVORES AVL",MB_OK | MB_ICONEXCLAMATION);
		}
		else
		{
			MessageBox(NULL,"O Nodo nao encontrado!!! ","ARVORES AVL",MB_OK | MB_ICONEXCLAMATION);
		}
	}
}

void MyGlutKeyboard(unsigned char Key, int x, int y)
{
	switch(Key)
	{
	case 27:
	case 'q':
		exit(0);
		break;
	};

	glutPostRedisplay();
}


void MyGlutMenu( int value )
{
	MyGlutKeyboard( value, 0, 0 );
}

void MyGlutIdle( void )
{
	if ( glutGetWindow() != main_window )
	glutSetWindow(main_window);
	glutPostRedisplay();
}


void MyGlutMouse(int button, int button_state, int x, int y )
{
}


void MyGlutMotion(int x, int y )
{
	glutPostRedisplay();
}

void MySetupRC(void)
{
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);  // cor branca de fundo da janela
}

void MyGlutReshape( int x, int y )
{
//	int tx, ty, tw, th;
//	GLUI_Master.get_viewport_area( &tx, &ty, &tw, &th );
//	glViewport( tx, ty, tw, th );

//	xy_aspect = (float)tw / (float)th;

	//gluLookAt (0,0,0,0,0,1, 0,1,0);

	glutPostRedisplay();
}

void MyGlutDisplay( void )
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLineWidth(3);

	DrawNodos(pNodoA, 0.0f, 0.9f, 0.0f, 0);

	glutSwapBuffers();
}


void main(int argc, char* argv[])
{
	glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
	glutInitWindowPosition( 50, 50 );
	glutInitWindowSize( 800, 600 );

	main_window = glutCreateWindow( "ARVORES AVL by Cleber Machado" );

	glutDisplayFunc( MyGlutDisplay );
	GLUI_Master.set_glutReshapeFunc( MyGlutReshape );
	GLUI_Master.set_glutKeyboardFunc( MyGlutKeyboard );
	GLUI_Master.set_glutSpecialFunc( NULL );
	GLUI_Master.set_glutMouseFunc( MyGlutMouse );
	glutMotionFunc( MyGlutMotion );

    printf ("\n      =====================================");
    printf ("\n        INF01124 - Classificacao e Pesquisa de Dados");
    printf ("\n        Cleber A. G. Machado");
    printf ("\n        Trabalho Pratico2 - Turmas 15:30");
    printf ("\n      =====================================\n");

	glui = GLUI_Master.create_glui_subwindow( main_window,GLUI_SUBWINDOW_BOTTOM );

	obj_arquivo= glui->add_panel( "Arquivo", 1);
	glui->add_button_to_panel(obj_arquivo, "Abrir", ABRIR_ID, control_cb );

	glui->add_column(false);
	obj_exibir = glui->add_panel( "Exibir", 1);
	obj_set_texto = glui->add_radiogroup_to_panel( obj_exibir, &set_texto, EXIBIR_ID, control_cb );
	glui->add_radiobutton_to_group(obj_set_texto,"Nome");
	glui->add_radiobutton_to_group(obj_set_texto,"FB");

	glui->add_column(false);
	obj_adcionar = glui->add_panel( "Adicionar", 1);
	glui->add_edittext_to_panel(obj_adcionar, "Nodo",GLUI_EDITTEXT_INT, &nodo_adcionar, 0, control_cb );
	glui->add_button_to_panel(obj_adcionar, "Adicionar", ADCIONAR_ID, control_cb );

	glui->add_column(false);
	obj_remover = glui->add_panel( "Remover", 1);
	glui->add_edittext_to_panel(obj_remover, "Nodo",GLUI_EDITTEXT_INT, &nodo_remover, 0, control_cb );
	glui->add_button_to_panel(obj_remover, "Remover", REMOVER_ID, control_cb );

	glui->add_column(false);
	obj_consultar = glui->add_panel( "Consultar", 1);
	glui->add_edittext_to_panel(obj_consultar, "Nodo",GLUI_EDITTEXT_INT, &nodo_consultar, 0, control_cb );
	glui->add_button_to_panel(obj_consultar, "Consultar", CONSULTAR_ID, control_cb );

	glui->add_column(false);
	glui->add_button( "Sair", 10,(GLUI_Update_CB)exit );

	glui->set_main_gfx_window( main_window );
	obj_glui = GLUI_Master.create_glui_subwindow( main_window,GLUI_SUBWINDOW_BOTTOM );
	obj_glui->set_main_gfx_window( main_window );

	GLUI_Master.set_glutIdleFunc( MyGlutIdle );

	MySetupRC();
	glutMainLoop();
}
