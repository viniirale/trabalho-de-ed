#include "AVL.h"

using namespace std;

bool carrega_csv(void)
{
    int info;
	unsigned long tamanho;
	char* pto;


	ifstream entrada("nodos.txt",ios_base::in);
	if (entrada == NULL)
	{
		cout << "Erro ao abrir nodos.txt";
		return NULL;
	}

	entrada.seekg(0,ios::end);
	tamanho=entrada.tellg();
	entrada.seekg(0,ios::beg);

	char arquivo_txt[1000];
	entrada.read((char *) &arquivo_txt, tamanho);
	entrada.close();

    pto=&arquivo_txt[0];
	int i=0;
    while((*pto!=NULL)&&(i<1000))
    {
		if( (*pto>47) && (*pto<58) && (*(pto+1)>47) && (*(pto+1)<58) )
		{
			info = 10*(*pto - 48);
			info+= (*(pto+1)-48);
			pNodoA = InsereAVL(pNodoA, info);
			pNodoA = CorrigeAVL(pNodoA);
			info=0;
			pto++;
		}
		pto++;
		i++;

    }
	Seta_FB(pNodoA);
	return true;
}
