#include<stdio.h>
#include<stdlib.h>
struct TNodoA{
int info;
struct TNodoA *esq;
struct TNodoA *dir;
};
typedef struct TNodoA pNodoA;
struct TNodoB
{
        int info;
        int FB;
        struct TNodoB *esq;
        struct TNodoB *dir;
};
typedef struct TNodoB pNodoB;
//aqui come�am as fun��es de abp
void controlaremoveABP(pNodoA **a, char nomearquivo[],unsigned long int *comp, long int*tempo);//remove determinado nodo da arvore
void removeab (pNodoA **tree, int num,unsigned long int *comp);//remove determinado nodo da arvore
pNodoA* InsereArvore(pNodoA *a, int ch,long int *comp);//insere nodo na arvore
pNodoA* controlainsereArvoreabp(pNodoA *a, char nomearquivo[],unsigned long int *comp,long int *tempo);
void Inserearquivoabp(pNodoA *a,int codigo,char nome[],int *flag,FILE *arq,unsigned long int *comp,long int *tempo);
void controlaconsulta(pNodoA **a,char nomearquivo[],unsigned long int *comp,long int *tempo);
void consulta(pNodoA **a,int ch, unsigned long int *comp);
//funcoes de avl retiradas de www.inf.ufrgs.br/~cagmachado/INF01124/t2.htm
pNodoB* InsereAVL(pNodoB* pNodo, int ch,unsigned long int *comp);
pNodoB* Consulta(pNodoB* pNodo, char ch);
pNodoB* Remove(pNodoB* pNodo, char ch);
pNodoB* RemoveAVL(pNodoB* pNodo);
int Calcula_FB(pNodoB* pNodo);
void Seta_FB(pNodoB* pNodo);
pNodoB* CorrigeAVL(pNodoB* pNodo);
pNodoB* rotacao_direita(pNodoB* pNodo);
pNodoB* rotacao_esquerda(pNodoB* pNodo);
pNodoB* rotacao_dupla_direita (pNodoB* pNodo);
pNodoB* rotacao_dupla_esquerda (pNodoB* pNodo);
//aqui termina
int Conta_nodo(pNodoA *a);//conta numero de nodos na arvore
pNodoA* cria_arvore(void);//fun��o que cria a arvore
void preFixado(pNodoA *a);//printa a arvore na tela na ordem  prefixada a esquerda
int maior(int a, int b);
pNodoA* destroi(pNodoA *a);
int altura(pNodoA *a);
