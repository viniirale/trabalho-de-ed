#include<stdio.h>
#include<stdlib.h>
#include"trabalho.h"
#include<time.h>
#include <locale.h>
#include<windows.h>
#include<limits.h>

pNodoA* cria_arvore(void)// inicia a arvore com a raiz
{
    return NULL;
}
pNodoA* controlainsereArvoreabp(pNodoA *a, char nomearquivo[],unsigned long int *comp,long int *tempo)//abre o arquivo com numeros a ser inserido e os envia para a função de inserção até o arquivo terminar
{
    int ch=0;
    FILE *arq;
    FILE *teste;
    *comp=0;
    *tempo=0;
    int I=0;


    clock_t start, end, elapsed; //para contar o tempo

    arq=fopen(nomearquivo,"r");
    teste=fopen("teste","w+");
    if(!arq)
        printf("erro na abertura do arquivo");
    else
    {
        start=clock(); //inicia a contagem do tempo;
        while(!feof(arq))
        {
            fscanf(arq,"%d\n",&ch);

            a=InsereArvore(a,ch,comp);
            //fprintf(teste,"%li\n",*comp);
        }
        end=clock(); //l� o tempo final
    }

    *tempo = 1000 * (end - start) / (CLOCKS_PER_SEC); //calcula o tempo decorrido em milissegundos
    fclose(arq);
    fclose(teste);

    return a;
}

pNodoA* InsereArvore(pNodoA *a, int ch,long int *comp)//função de inserção de nodos da abp
{
    if (a == NULL)
    {
        a = (pNodoA*) malloc(sizeof(pNodoA));
        a->info = ch;
        a->esq = NULL;
        a->dir = NULL;
    }
    else if (ch < (a->info))
    {
        *comp+=1;
        a->esq = InsereArvore(a->esq,ch,comp);
    }
    else
    {
        *comp+=1;
        a->dir = InsereArvore(a->dir,ch,comp);
    }
    return a;
}
void controlaremoveABP(pNodoA **a, char nomearquivo[],unsigned long int *comp, long int*tempo)//controla os nodos q devem ser removidos, lendoos do arquivo e enviando pra remoção
{
    FILE *arq;          //ponteiro do arquivo
    int ch=0;
    *comp=0;
    *tempo=0;
    clock_t start, end, elapsed; //para contar o tempo

    arq=fopen(nomearquivo,"r");
    if(a==NULL)
        printf("arvore vazia");
    else
    {
        start=clock(); //inicia a contagem do tempo;
        while(!feof(arq))
        {
            fscanf(arq,"%d\n",&ch);
            removeab(a,ch,comp);
        }
        end=clock(); //l� o tempo final
    }
     *tempo = 1000 * (end - start) / (CLOCKS_PER_SEC); //calcula o tempo decorrido em milissegundos
    fclose(arq);
}
//retirada de http://equipe.nce.ufrj.br/adriano/c/apostila/arvore.htm#remocao
void removeab (pNodoA **tree, int num,unsigned long int *comp)
{
    pNodoA *p,  /* p aponta para o no a ser removido */
           *q,  /* q aponta para o pai do no */
           *rp, /* rp aponta que ira substituir o no p */
           *f,
           *s;  /* sucessor do no p */

    p = *tree;
    q=NULL;

    /* procura o no com a chave num, p aponta para o no
    e q aponta para o pai do no */

    do
    {
        //printf("iuahuas");
        q = p;
        if ( num < p->info)
        {
            p = p->esq;
            *comp+=1;
        }
        else
        {
             p = p->dir;
            *comp+=1;
        }

    }while ( p->esq!=NULL &&p->dir!=NULL&&  p->info != num); /* fim do while */
    if (!p)  /* a chave nao existe na arvore */
    {

    }
    else
    {
        /* agora iremos ver os dois primeiros casos, o no tem um filho
           no maximo */
        if (p->esq == NULL)
            rp = p->dir;
        else if (p->dir == NULL)
            rp = p->esq;
        else
        {
            f=p;
            rp = p->dir;
            s = rp->esq;   /* s e sempre o filho esq de rp */
            while (s != NULL)
            {
                f = rp;
                rp = s;
                s = rp->esq;
            }
            /* neste ponto, rp e o sucessor em ordem de p */
            if (f != p)
            {
                /*  p nao e o pai de rp e rp == f->left */
                f->esq = rp->dir;
                /* remove o no rp de sua atual posicao e o
                   substitui pelo filho direito de rp
                   rp ocupa o lugar de p
                */
                rp->dir = p->dir;
            }
            /* define o filho esquerdo de rp de modo que rp
               ocupe o lugar de p
            */
            rp->esq = p->esq;
        }
        /* insere rp na posicao ocupada anteriormente por p */
        if (q == NULL)
            *tree = rp;
        else if (p == q->esq)
            q->esq = rp;
        else
            q->dir = rp;
        free(p);
    }
}
pNodoA* destroi(pNodoA *a)
{
    if (a!= NULL)
    {
        destroi(a->esq);
        destroi(a->dir);
        //printf("destruindo",a->info);
        free(a);
    }
    return NULL;
}
void controlaconsulta(pNodoA **a,char nomearquivo[],unsigned long int *comp,long int *tempo)
{
    FILE *arq;          //ponteiro do arquivo
    int ch=0;
    *comp=0;
     *tempo=0;
    clock_t start, end, elapsed; //para contar o tempo

    arq=fopen(nomearquivo,"r");
    if(a==NULL)
        printf("arvore vazia");
    else
    {
        start=clock(); //inicia a contagem do tempo;
        while(!feof(arq))
        {
            fscanf(arq,"%d\n",&ch);
            consulta(a,ch,comp);
        }
        end=clock(); //l� o tempo final
    }
    *tempo = 1000 * (end - start) / (CLOCKS_PER_SEC); //calcula o tempo decorrido em milissegundos
    fclose(arq);
}
void consulta(pNodoA **a,int ch,unsigned long int *comp)
{
    pNodoA *p;

    p=*a;
    while (p!=NULL)
    {
        if (p->info == ch )
        {
            *comp+=1;
            return 0; //achou retorna o ponteiro para o nodo
        }
        else if (p->info > ch)
        {
            *comp+=1;
            p = p->esq;
        }
        else
        {
            p = p->dir;
        }
    }

}
int Conta_nodo(pNodoA *a)
{
    if (a!= NULL)
    {

        return 1+Conta_nodo(a->esq)+Conta_nodo(a->dir);
    }
    else
        return 0;
}

void preFixado(pNodoA *a)
{
    if (a!= NULL)
    {
        printf("%d\n",a->info);
        preFixado(a->esq);
        preFixado(a->dir);
    }
}
void Inserearquivoabp(pNodoA *a,int codigo,char nome[],int *flag,FILE *arq,unsigned long int *comp,long int *tempo)

{


     setlocale(LC_ALL,""); //para imprimir corretamente na tela os caracteres acentuados
    int rot=0;
    if(*flag==0)
    {
        fprintf(arq,"**********VERSÃO COM ABP**********\n");
        *flag=1;
    }
    else
    {
        fprintf(arq,"----------------------------------\n");
    }
    switch(codigo)
    {
    case 1:
        fprintf(arq,"Inserindo dados do arquivo %s\n",nome);
        break;
    case 2:
        fprintf(arq,"Consultando dados do arquivo %s\n",nome);
        break;
    case 3:
        fprintf(arq,"Removendo dados do arquivo %s\n",nome);
        break;

    }

    fprintf(arq,"**********ESTATÍSTICAS ABP**********\n");

    fprintf(arq,"Tempo: %li ms\n",*tempo);
    fprintf(arq,"Nodos: %d\n",Conta_nodo(a));
    fprintf(arq,"Altura: %d\n",altura(a));
    fprintf(arq,"Fator: %d\n",((altura(a->dir))- (altura(a->esq))));


    fprintf(arq,"Comparações: %lu\n",*comp);
    fprintf(arq,"Rotações: %d\n",rot);

}


//funcoes de avl****************************************************************************
int altura(pNodoA *a)
{
    int Alt_Esq, Alt_Dir;
	if (a == NULL)
        return 0;
	else
	{
	    Alt_Dir = altura (a->dir);
		Alt_Esq = altura (a->esq);
		if (Alt_Esq >= Alt_Dir)
		{
			return (1 + Alt_Esq);
		}
		else
		{
			return (1 + Alt_Dir);
		}
	}
}


/*
int Calcula_FB(pNodoB* pNodo)
{
    if(pNodo == NULL)return 0;
    return ( altura(pNodo->dir)-altura(pNodo->esq));
}


void Seta_FB(pNodoB* pNodo)
{
     if (pNodo!= NULL)
     {
		 pNodo->FB=(altura(pNodo->esq)-altura(pNodo->dir) );
		 Seta_FB(pNodo->esq);
		 Seta_FB(pNodo->dir);
     }
}


pNodoB* rotacao_direita(pNodoB* N3)
{
       pNodoB* N2= N3->esq;
       if(N2->dir) N3->esq = N2->dir;
       else N3->esq=NULL;
       N2->dir=N3;
       return N2;
}

pNodoB* rotacao_esquerda(pNodoB* N1)
{
       pNodoB* N2= N1->dir;
       if(N2->esq) N1->dir= N2->esq;
       else N1->dir=NULL;
       N2->esq=N1;
       return N2;
}

pNodoB* rotacao_dupla_direita (pNodoB* N3)
{
       pNodoB* N1= N3->esq;
       pNodoB* N2= N1->dir;

       if(N2->esq) N1->dir= N2->esq;
       else N1->dir=NULL;

       if(N2->dir) N3->esq = N2->dir;
       else N3->esq=NULL;

       N2->esq=N1;
       N2->dir=N3;

       return N2;
}

pNodoB* rotacao_dupla_esquerda (pNodoB* N1)
{
       pNodoB* N3= N1->dir;
       pNodoB* N2= N3->esq;

       if(N2->esq) N1->dir= N2->esq;
       else N1->dir=NULL;

       if(N2->dir) N3->esq = N2->dir;
       else N3->esq=NULL;

       N2->esq=N1;
       N2->dir=N3;

       return N2;
}

pNodoB* CorrigeAVL(pNodoB* pNodo)
{
	if(pNodo != NULL)
	{
		pNodo->FB=Calcula_FB(pNodo);
		if(pNodo->FB == 2)
		{
			pNodo->esq->FB=Calcula_FB(pNodo->esq);
			if(pNodo->esq->FB > 0)
			{
				pNodo = rotacao_direita(pNodo);
			}
			else
			{
				pNodo =  rotacao_dupla_direita(pNodo);
			}
		}
		else if(pNodo->FB == -2)
		{
			pNodo->dir->FB=Calcula_FB(pNodo->dir);
			if(pNodo->dir->FB < 0)
			{
				pNodo = rotacao_esquerda(pNodo);
			}
			else
			{
				pNodo =  rotacao_dupla_esquerda(pNodo);
			}
		}
		pNodo->esq = CorrigeAVL(pNodo->esq);
		pNodo->dir = CorrigeAVL(pNodo->dir);
    }
    return pNodo;
}


pNodoB* InsereAVL(pNodoB* pNodo, int ch)
{
	if (pNodo == NULL)
	{
		pNodo =(pNodoB*)malloc(sizeof(pNodoB));
		pNodo->info= ch;
		pNodo->FB= 0;
		pNodo->esq= NULL;
		pNodo->dir= NULL;
		return pNodo;
	}
	else
	{
		if(ch < pNodo->info)
		{
			pNodo->esq = InsereAVL(pNodo->esq,ch);
		}
		else
		{
			pNodo->dir = InsereAVL(pNodo->dir,ch);
		}
	}
	return pNodo;
}





pNodoB* Consulta(pNodoB* pNodo, char ch)
{
    while (pNodo != NULL)
    {
		if(ch == pNodo->info)
		{
			return pNodo;
		}
		else
		{
			if(ch < pNodo->info)
			{
				pNodo=pNodo->esq;
			}
			else
			{
				pNodo=pNodo->dir;
			}
		}
	}
	return NULL;
}

pNodoB *controlainsereArvoreavl(pNodoB *avl,char nomearqoperado[],int **comp,int *tempo,int **rot)
{
    int ch=0;
    FILE *arq;
    **comp=0;
    **rot=0;
    *tempo=0;
    clock_t start, end, elapsed; //para contar o tempo

    arq=fopen(nomearquivo,"r");
    if(!arq)
        printf("erro na abertura do arquivo");
    else
    {
        start=clock(); //inicia a contagem do tempo;
        while(!feof(arq))
        {
            fscanf(arq,"%d\n",&ch);
            avl=InsereAVL(a,ch,comp);
        }
        end=clock(); //l� o tempo final
    }
    *tempo = 1000 * (end - start) / (CLOCKS_PER_SEC); //calcula o tempo decorrido em milissegundos
    fclose(arq);
    return a;
}


pNodoB* Remove(pNodoB* pNodo, char ch)
{
	pNodoB* pNodoAux=pNodo;
	pNodoB* pNodoPai;
	int bdir=0;

	if(ch == pNodo->info) return RemoveAVL(pNodoAux);

    while (pNodoAux != NULL)
    {
		if(ch == pNodoAux->info)
		{
			if(bdir) pNodoPai->dir= RemoveAVL(pNodoAux);
			else pNodoPai->esq= RemoveAVL(pNodoAux);
			return pNodo;
		}
		else
		{
			if(ch < pNodoAux->info)
			{
				bdir=0;
				pNodoPai=pNodoAux;
				pNodoAux=pNodoAux->esq;
			}
			else
			{
				bdir=1;
				pNodoPai=pNodoAux;
				pNodoAux=pNodoAux->dir;
			}
		}
	}
	return pNodo;
}


pNodoB* RemoveAVL(pNodoB* pNodo)
{
	pNodoB* pNodoAux;
	pNodoB* pNodoAuxPai;

	if((pNodo->esq == NULL) && (pNodo->dir == NULL))
	{
        free(pNodo);
    	return NULL;
	}
	else if((pNodo->esq == NULL) && (pNodo->dir != NULL))
	{
		pNodoAux=pNodo->dir;
        free(pNodo);
    	return pNodoAux;
	}
	else if((pNodo->esq != NULL) && (pNodo->dir == NULL))
	{
		pNodoAux=pNodo->esq;
		free(pNodo);
		return pNodoAux;
	}
	else
	{
		if(pNodo->esq->dir == NULL)
        {
			pNodoAux=pNodo->esq;
    	    pNodo->esq->dir=pNodo->dir;
            free(pNodo);
    		return pNodoAux;
        }
        else
        {
    		pNodoAux=pNodo->esq;

    		while (pNodoAux->dir != NULL)
    		{
    			pNodoAuxPai=pNodoAux;
                pNodoAux=pNodoAux->dir;
    		}

    		if(pNodoAux->esq!=NULL) pNodoAuxPai->dir=pNodoAux->esq;
    		else pNodoAuxPai->dir=NULL;

            pNodoAux->dir=pNodo->dir;
    	    pNodoAux->esq=pNodo->esq;

    		free(pNodo);
    		return pNodoAux;
        }
	}

	return NULL;
}
*/
